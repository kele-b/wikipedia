import markdown
from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from . import util


class NewEntryForm(forms.Form):
    title = forms.CharField(widget=forms.TextInput(attrs={'style': 'width:100%;'}))
    content = forms.CharField(widget=forms.Textarea())


def index(request):
    return render(request, "encyclopedia/index.html", {
        "entries": util.list_entries()
    })


def get(request, title):

    # If requested entry does not exist, error page will be displayed with status code 404
    if util.get(title) is None:
        return render(request, "encyclopedia/error.html", {
            "message": f"The page with name {title} does not exist in encyclopedia!",
        }, None, 404)

    # Converting Markdown file to HTML
    content = markdown.markdown(util.get(title))
    return render(request, "encyclopedia/entry.html", {
        "title": title.upper(),
        "content": content
    })


def create(request):

    if request.method == "POST":
        form = NewEntryForm(request.POST)

        if form.is_valid():
            title = form.cleaned_data["title"]
            content = form.cleaned_data["content"]

            # If encyclopedia entry does not exist, new entry will be created
            if util.get(title) is None:
                util.save_entry(title, content)
                return HttpResponseRedirect(reverse('title', kwargs={'title': title}))

            else:
                # If encyclopedia entry already exist, error page will be displayed with status code 400
                return render(request, "encyclopedia/error.html", {
                    "message": f"The page with name {title} already exist in encyclopedia!"
                }, None, 400)

    # If request method is GET
    return render(request, "encyclopedia/new_page.html", {
        "form": NewEntryForm()
    })
